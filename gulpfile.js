const gulp = require("gulp");
const babel = require("gulp-babel");

const path = "./src/backend/**/*.js";

gulp.task("transpile", () =>
  gulp
    .src(path)
    .pipe(babel())
    .pipe(gulp.dest("dist"))
);

gulp.watch(path, ['transpile']);

gulp.task('default', ['transpile'])
