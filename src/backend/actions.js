import axios from 'axios';
import { token } from './config';

const randomTill = (n) => Math.floor(Math.random() * Math.floor(n));

export const sendMessage = (message, chatID, res) => {
  axios
    .post(
      `https://api.telegram.org/bot${token}/sendMessage`,
      {
        chat_id: chatID,
        text: message
      }
    )
    .then(() => {
      console.log('Message posted')
      res.end('ok')
    })
    .catch(err => {
      console.log('Error :', err)
      res.end('Error :' + err)
    });
};

export const sendCoube = (chatID, res) => {
  sendMessage('Lets find some...', chatID, res);
  axios
    .get('https://coub.com/api/v2/search/coubs?q=anime')
    .then(result => {
      if (result.data && result.data.coubs && result.data.coubs.length) {
        sendMessage(
          `https://coub.com/view/${result.data.coubs[randomTill(result.data.coubs.length)].permalink}`,
          chatID, res
        )
      } else {
        res.end();
      }
    })
    .catch(err => {
      console.log('Error :', err)
      res.end('Error :' + err)
    });;
}