import express from 'express';
import bodyParser from 'body-parser';
import { sendMessage, sendCoube } from './actions';
import { port } from './config';

const app = express();

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.post('/new-message', function(req, res) {
  const { message } = req.body;

  console.log('--->\n', message, '\n<---');

  if (!message || !message.text) {
    return res.end();
  } else {
     if (message.text[0] === '/') {
      const chatId = message.chat.id;

      switch(message.text) {
        case '/whoru': sendMessage('I am the Bot and I fucked your rot!', chatId, res); break;
        case '/rostyk': sendMessage('@IamFieryCat is kawaiii!', chatId, res); break;
        case '/dev': sendMessage('@BinaryCry!', chatId, res); break;
        case '/cmd': sendMessage('whoru, rostyk, dev, cmd, anthem, hentai, coube', chatId, res); break;
        case '/hentai': sendMessage('https://coub.com/view/1cmojk', chatId, res); break;
        case '/anthem': sendMessage('https://youtu.be/EdH1d1UMKPE', chatId, res); break;
        case '/coube': sendCoube(chatId, res); break;
        default: sendMessage('Unknown command', chatId, res);
      }

    } else {
      const chatId = message.chat.id;
      if (message.text.match(/нет/i)) {
        sendMessage('Пидора ответ!', chatId, res);
      } else {
        res.end();
      }
    }
  }

});

app.listen(port, function() {
  console.log('Started');
})